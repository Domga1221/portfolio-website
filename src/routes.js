import Home from './components/Home/Home.vue'
import Blog from './components/Blog/Blog.vue'
import CardGameArticle from './components/Blog/Articles/CardGame/CardGameArticle.vue'
import VisualDesignArticle from './components/Blog/Articles/Dota/VisualDesignArticle.vue'
import LevelDesignArticle from './components/Blog/Articles/LevelDesign/LevelDesignArticle.vue'
import SoundDesignArticle from './components/Blog/Articles/SoundDesign/SoundDesignArticle.vue'
import DOSArticle from './components/Blog/Articles/DOS/DOSArticle.vue'

export default[
    { path: '/portfolio-website', component: Home, meta: { title: 'Dominik Portfolio'}},
    { path: '/portfolio-website/blog', component: Blog, meta: { title: 'Dominik Blog'}},
    { path: '/portfolio-website/blog/cardgame', component: CardGameArticle, meta: { title: 'Creating a card game from scratch!'} },
    { path: '/portfolio-website/blog/dotavisualdesign', component: VisualDesignArticle, meta: { title: 'Dota 2 Visual Analysis'} },
    { path: '/portfolio-website/blog/leveldesign', component: LevelDesignArticle, meta: { title: 'Narrative Level Design with Cubes'} },
    { path: '/portfolio-website/blog/sounddesign', component: SoundDesignArticle, meta: { title: `Reimagining Tekken 7's Audio`} },
    { path: '/portfolio-website/blog/divinity', component: DOSArticle, meta: { title: `Creating Divinity: Original Sin's Combat in Unity`}},
    { path: '*', redirect: '/portfolio-website' }
]